# -*- coding: utf-8 -*-
from django import template
from dashboard.models import SettingsInDatabase
register = template.Library()


@register.simple_tag()
def view_rate():
    rate_setting = SettingsInDatabase.objects.filter(unique_key='RATE').values('value').first()
    return u"1 BTC = {} USD".format(rate_setting.get('value')) if rate_setting else 'None'
