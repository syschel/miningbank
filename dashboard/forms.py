from django.contrib.auth.forms import ReadOnlyPasswordHashField, UserChangeForm as AuthUserChangeForm
from django import forms
from django.utils.translation import ugettext_lazy as _
from django.utils.safestring import mark_safe
from django.conf import settings
from django.contrib import messages
from dashboard.models import User


class UserCreationForm(forms.ModelForm):
    """A form for creating new users. Includes all the required
    fields, plus a repeated password."""
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Password confirmation', widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ('email', 'country')

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")
        return password2

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
            subject = _('Reg subject')
            message = _('Registration text')
            user.email_user(subject, message)
        return user


class UserChangeForm(forms.ModelForm):
    """A form for updating users. Includes all the fields on
    the user, but replaces the password field with admin's
    password hash display field.
    """
    password = ReadOnlyPasswordHashField()

    class Meta:
        model = User
        fields = ('email', 'password', 'country')

    def clean_password(self):
        # Regardless of what the user provides, return the initial value.
        # This is done here, rather than on the field, because the
        # field does not have access to the initial value
        return self.initial["password"]


class UserChangeAdminForm(AuthUserChangeForm):
    """
    Admin panel edit form
    """
    class Meta:
        model = User
        fields = '__all__'


class RePasswordForm(forms.Form):
    email = forms.EmailField(required=True)

    def __init__(self, *args, **kwargs):
        super(RePasswordForm, self).__init__(*args, **kwargs)
        self.fields['email'].widget.attrs['placeholder'] = 'youremail@me.com'

    def clean(self):
        email = self.cleaned_data.get('email', '')
        self.user = None
        users = User.objects.filter(email=email)
        for user in users:
            self.user = user
        if self.user is None:
            raise forms.ValidationError('Email not found')
        return self.cleaned_data


class HorizontalRadioRenderer(forms.RadioSelect.renderer):
    def render(self):
        return mark_safe(u'\n'.join([u'%s\n' % w for w in self]))


class SettingsForm(forms.ModelForm):

    old_pass = forms.CharField(label=_('Old Password'), max_length=128, required=False,
                               widget=forms.PasswordInput(render_value=True))
    new_pass = forms.CharField(label=_('New Password'), max_length=128, required=False,
                               widget=forms.PasswordInput(render_value=True))
    new_pass2 = forms.CharField(label=_('Retype New Password'), max_length=128, required=False,
                                widget=forms.PasswordInput(render_value=True))

    def __init__(self, *args, **kwargs):
        self.user = kwargs.get('instance')
        super(SettingsForm, self).__init__(*args, **kwargs)
        # self.fields['auto_payout'].widget.attrs['class'] = 'radio'
        self.fields['old_pass'].widget.attrs['autocomplete'] = 'off'
        self.fields['new_pass'].widget.attrs['autocomplete'] = 'off'
        self.fields['new_pass2'].widget.attrs['autocomplete'] = 'off'
        self.fields['country'].widget.attrs['autocomplete'] = 'off'
        self.fields['wallet'].widget.attrs['autocomplete'] = 'off'

        self.fields['country'].widget.attrs['class'] = 'form-control'
        self.fields['wallet'].widget.attrs['class'] = 'form-control'
        self.fields['old_pass'].widget.attrs['class'] = 'form-control'
        self.fields['new_pass'].widget.attrs['class'] = 'form-control'
        self.fields['new_pass2'].widget.attrs['class'] = 'form-control'

    def clean(self):
        cleaned_data = self.cleaned_data
        # change  password
        if cleaned_data['old_pass']:
            if not self.user.check_password(cleaned_data['old_pass']):
                raise forms.ValidationError({'old_pass': _('Password incorrect')}, code='invalid')
            else:
                if not cleaned_data['new_pass'] and not cleaned_data['new_pass2']:
                    raise forms.ValidationError({'new_pass': _('This field is required.'),
                                                 'new_pass2': _('This field is required.')}, code='required')
                elif not cleaned_data['new_pass']:
                    raise forms.ValidationError({'new_pass': _('This field is required.')}, code='required')
                elif not cleaned_data['new_pass2']:
                    raise forms.ValidationError({'new_pass2': _('This field is required.')}, code='required')
                elif cleaned_data['new_pass'] != cleaned_data['new_pass2']:
                    raise forms.ValidationError({'new_pass2': _('Passwords do not match')}, code='invalid')
        return cleaned_data

    class Meta:
        model = User
        widgets = {'auto_payout': forms.RadioSelect(renderer=HorizontalRadioRenderer)}
        fields = ('country', 'auto_payout', 'wallet')
