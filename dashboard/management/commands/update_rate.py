# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
from dashboard.tools import rete_update


class Command(BaseCommand):
    help = 'Update rate BTC to USD on API'

    def handle(self, *args, **options):
        rete_update()
