from django.shortcuts import render
from django.core.urlresolvers import reverse, reverse_lazy
from django.views.generic.edit import FormView, UpdateView, CreateView
from django.views.generic.base import RedirectView, TemplateView
from django.views.generic import DetailView, ListView, View
from django.contrib.auth.forms import AuthenticationForm, PasswordResetForm
from django.contrib.auth import login, logout
from django.contrib.auth.hashers import make_password
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth import authenticate
from django.contrib.auth.tokens import default_token_generator
from django.conf import settings
from django.contrib import messages

from dashboard.forms import UserCreationForm, UserChangeForm, RePasswordForm, SettingsForm
from dashboard.models import User, Pages


class HomePage(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super(HomePage, self).get_context_data(**kwargs)
        return context


class LogIn(FormView):
    template_name = 'login.html'
    form_class = AuthenticationForm
    success_url = reverse_lazy('dashboard')

    def form_valid(self, form):
        form.get_user().backend = 'django.contrib.auth.backends.ModelBackend'
        login(self.request, form.get_user())
        return super(LogIn, self).form_valid(form)


class SignUp(CreateView):
    template_name = 'signup.html'
    form_class = UserCreationForm
    success_url = reverse_lazy('login')

    def form_valid(self, form):
        messages.info(self.request,
                      _('Thank you for registering. Now you can log in using your email and password.'))
        return super(SignUp, self).form_valid(form)


class LogoutView(RedirectView):
    permanent = False
    url = reverse_lazy('login')

    def dispatch(self, request, *args, **kwargs):
        try:
            logout(request)
            messages.success(self.request,
                             _('Goodbye.'))
        except logout:
            pass
        return super(LogoutView, self).dispatch(request, *args, **kwargs)


class RePassword(FormView):
    template_name = 'repaswd.html'
    form_class = RePasswordForm
    success_url = reverse_lazy('login')

    def form_valid(self, form):
        form.user.backend = 'django.contrib.auth.backends.ModelBackend'
        new_pass = User.objects.make_random_password()
        form.user.set_password(new_pass)
        form.user.save()
        message = _('Your new password at {}:'.format(settings.SITE_DOMAIN)) + ' %s' % new_pass
        form.user.email_user(_('{}: New password'.format(settings.SITE_DOMAIN)), message)
        messages.warning(self.request,
                         _('We send new password to your registered email address.'))
        return super(RePassword, self).form_valid(form)


class PagesView(DetailView):
    template_name = 'page.html'
    model = Pages


class DashboardView(TemplateView):
    template_name = 'dashboard.html'

    def get_context_data(self, **kwargs):
        context = super(DashboardView, self).get_context_data(**kwargs)
        context['dashboard_active'] = True
        return context


class ShopView(TemplateView):
    template_name = 'shop.html'

    def get_context_data(self, **kwargs):
        context = super(ShopView, self).get_context_data(**kwargs)
        context['shop_active'] = True
        return context


class HistoryView(TemplateView):
    template_name = 'history.html'

    def get_context_data(self, **kwargs):
        context = super(HistoryView, self).get_context_data(**kwargs)
        context['history_active'] = True
        return context


class PayoutsView(TemplateView):
    template_name = 'payouts.html'

    def get_context_data(self, **kwargs):
        context = super(PayoutsView, self).get_context_data(**kwargs)
        context['payouts_active'] = True
        return context


class SettingsView(UpdateView):
    template_name = 'settings.html'
    form_class = SettingsForm
    model = User
    success_url = reverse_lazy('settings')

    def get_object(self, queryset=None):
        return self.request.user

    def get_context_data(self, **kwargs):
        context = super(SettingsView, self).get_context_data(**kwargs)
        context['settings_active'] = True
        return context

    def form_valid(self, form):
        clean = form.cleaned_data
        if clean.get('new_pass2'):
            form.user.password = make_password(clean.get('new_pass2'))
            form.user.save()
            form.user.backend = 'django.contrib.auth.backends.ModelBackend'
            login(self.request, form.user)
        messages.success(self.request, _('Change compiled.'))
        return super(SettingsView, self).form_valid(form)
