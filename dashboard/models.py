from django.db import models
from django.contrib.auth.base_user import BaseUserManager
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.base_user import AbstractBaseUser
from django.utils.translation import ugettext_lazy as _
from django.core.mail import send_mail
from django.core.urlresolvers import reverse


class UserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """
        Creates and saves a User with the given email and password.
        """
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        extra_fields.setdefault('is_superuser', False)
        extra_fields.setdefault('is_staff', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_panel', True)

        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_staff=True.')

        return self._create_user(email, password, **extra_fields)


class User(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(_('email address'), unique=True)
    country = models.CharField(_('country'), max_length=50, blank=True, null=True)
    date_joined = models.DateTimeField(_('date joined'), auto_now_add=True)
    is_active = models.BooleanField(_('active'), default=True)
    is_staff = models.BooleanField(_('staff status'), default=False)
    is_panel = models.BooleanField(_('paid access'), default=False)

    wallet = models.CharField(_('wallet'), max_length=50, blank=True, null=True)

    AUTOPAYOUT0, AUTOPAYOUT1, AUTOPAYOUT2, AUTOPAYOUT3 = range(0, 4)
    AUTOPAYOUTS = (
        (AUTOPAYOUT0, _(u"Автодокупить")),
        (AUTOPAYOUT1, _(u'Храним')),
        (AUTOPAYOUT2, _(u'Выводим месяц')),
        (AUTOPAYOUT3, _(u'Выводим неделя')),
    )
    auto_payout = models.IntegerField(_(u'Auto payout'), choices=AUTOPAYOUTS, default=AUTOPAYOUT0)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['country']

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def get_full_name(self):
        full_name = self.email
        return full_name.strip()

    def get_short_name(self):
        return self.email

    def email_user(self, subject, message, from_email=None, **kwargs):
        send_mail(subject, message, from_email, [self.email], **kwargs)

    def __str__(self):
        return self.email


class Pages(models.Model):
    """
    static pages or news or help
    """

    create          = models.DateTimeField(_(u"create"), auto_now_add=True)
    show            = models.BooleanField(_(u'show'), default=True)

    name            = models.CharField(_(u'name'), max_length=255, null=True, blank=False)
    text            = models.TextField(_(u'text'), null=True, blank=True)

    def __str__(self):
        return u"%s" % self.name

    def get_absolute_url(self):
        return reverse('page', args=[self.pk])

    class Meta:
        verbose_name = 'Pages'
        verbose_name_plural = 'Pages'
        ordering = ('-create',)


class SettingsInDatabase(models.Model):
    """
    Setting in database
    """
    name = models.CharField(_(u'name'), max_length=255, null=True, blank=False)
    UNKEYS = (
        ('DMFP1', _(u"Daily maintenance fee per 1 Th/s")),
        ('DGP1', _(u'Daily gross revenue per 1 Th/s')),
        ('BPM', _(u'Balance %')),
        ('ATP', _(u'Total power (?)Th/s')),
        ('POU', _(u'The price for 0.01 Th/s')),
        ('RATE', _(u'btc-e.nz - 1BTC=(?)USD')),
    )
    unique_key = models.CharField(_(u'unique key'), max_length=10, choices=UNKEYS, null=True, blank=True, unique=True)
    value = models.CharField(_(u'value'), max_length=255, null=True, blank=False, help_text=_('255 character'))
    attribute = models.CharField(_(u'attribute'), max_length=50, null=True, blank=True, help_text=_('50 character'))

    def __str__(self):
        return u"%s" % self.unique_key

    class Meta:
        verbose_name = 'Settings'
        verbose_name_plural = 'Settings'
