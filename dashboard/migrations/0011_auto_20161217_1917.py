# -*- coding: utf-8 -*-
# Generated by Django 1.10.2 on 2016-12-17 19:17
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dashboard', '0010_auto_20161216_2102'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='settingsindatabase',
            name='name',
        ),
        migrations.AlterField(
            model_name='settingsindatabase',
            name='unique_key',
            field=models.IntegerField(choices=[(0, 'Автодокупить'), (1, 'Храним'), (2, 'Выводим месяц'), (3, 'Выводим неделя')], null=True, unique=True, verbose_name='unique key'),
        ),
    ]
