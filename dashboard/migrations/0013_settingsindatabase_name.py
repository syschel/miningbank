# -*- coding: utf-8 -*-
# Generated by Django 1.10.2 on 2016-12-17 19:23
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dashboard', '0012_auto_20161217_1919'),
    ]

    operations = [
        migrations.AddField(
            model_name='settingsindatabase',
            name='name',
            field=models.CharField(max_length=255, null=True, verbose_name='name'),
        ),
    ]
