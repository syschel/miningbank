# -*- coding: utf-8 -*-
# Generated by Django 1.10.2 on 2016-12-16 19:53
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dashboard', '0007_auto_20161215_1412'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='wallet',
            field=models.CharField(blank=True, max_length=50, null=True, verbose_name='wallet'),
        ),
    ]
