from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from dashboard.models import User, Pages, SettingsInDatabase
from dashboard.forms import UserCreationForm, UserChangeAdminForm


class UserAdmin(BaseUserAdmin):
    # The forms to add and change user instances
    form = UserChangeAdminForm
    add_form = UserCreationForm

    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    list_display = ('email', 'country', 'is_panel', 'is_staff', 'is_superuser')
    list_filter = ('is_panel', 'is_staff', 'is_superuser')
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        ('Personal info', {'fields': ('country',)}),
        ('Permissions', {'fields': ('is_staff', 'is_superuser')}),
    )
    # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
    # overrides get_fieldsets to use this attribute when creating a user.
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'country', 'password1', 'password2')}
        ),
    )
    search_fields = ('email',)
    ordering = ('email',)
    filter_horizontal = ()


class SettingsInDatabaseAdmin(admin.ModelAdmin):
    list_display = ('name', 'unique_key', 'value', 'attribute')
    list_display_links = ('unique_key', 'name')
    search_fields = ('unique_key',)
    actions = None

    def has_delete_permission(self, request, obj=None):
        return False



admin.site.register(User, UserAdmin)
admin.site.register(Pages)
admin.site.register(SettingsInDatabase, SettingsInDatabaseAdmin)
