from django.conf import settings
from urllib.request import urlopen
import json
from dashboard.models import SettingsInDatabase


def rete_update():
    """Udate rate BTC in USD on API"""
    url_api = settings.BTC_RATE_API
    print('-= url open =-')
    response = urlopen(url_api)
    print('-= url read =-')
    frc = response.read()
    if len(frc) > 11:
        print('-= url decode =-')
        frc = frc.decode('utf-8')
        rate_json = json.loads(frc)
        end_txt = rate_json.get('ticker')
        print('-= rate_setting search =-')
        rate_setting, created = SettingsInDatabase.objects.get_or_create(unique_key='RATE')
        rate_setting.value = end_txt.get('sell')
        rate_setting.save()
        print('-= rate_setting save =-')
