"""conf URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from dashboard.views import HomePage, LogIn, RePassword, SignUp, LogoutView, PagesView, DashboardView, ShopView, HistoryView, PayoutsView, SettingsView
from django.contrib.auth.views import password_reset

urlpatterns = [
    url(r'^$', HomePage.as_view(), name='home'),
    url(r'^login/$', LogIn.as_view(), name='login'),
    url(r'^logout/$', LogoutView.as_view(), name='logout'),
    url(r'^repaswd/$', RePassword.as_view(), name='repaswd'),
    # url(r'^repaswd/$', password_reset, {'template_name': 'repaswd.html'}, name='repaswd'),
    url(r'^signup/$', SignUp.as_view(), name='signup'),
    url(r'^dashboard/$', DashboardView.as_view(), name='dashboard'),
    url(r'^shop/$', ShopView.as_view(), name='shop'),
    url(r'^history/$', HistoryView.as_view(), name='history'),
    url(r'^payouts/$', PayoutsView.as_view(), name='payouts'),
    url(r'^settings/$', SettingsView.as_view(), name='settings'),

    url(r'^p/(?P<pk>\d+)/$', PagesView.as_view(), name='page'),

    url(r'^admin/', admin.site.urls),
    # url(r'^auth/', include('django.contrib.auth.urls')),
]
