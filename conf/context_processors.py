from django.conf import settings


def cont_proc(request):
    return {
        'FROM_EMAIL': settings.FROM_EMAIL,
        'SITE_DOMAIN': settings.SITE_DOMAIN
    }
